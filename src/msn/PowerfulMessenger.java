package msn;

import net.sf.jml.MsnContact;
import net.sf.jml.MsnList;
import net.sf.jml.MsnMessenger;
import net.sf.jml.MsnSwitchboard;
import net.sf.jml.MsnUserStatus;
import net.sf.jml.event.MsnAdapter;
import net.sf.jml.event.MsnSwitchboardAdapter;
import net.sf.jml.message.MsnControlMessage;
import net.sf.jml.message.MsnInstantMessage;

/**
 * @author Roger Chen
 */
public class PowerfulMessenger extends BasicMessenger {

    private static class PowerfulListener extends MsnAdapter {

        public void contactListInitCompleted(MsnMessenger messenger) {
            // get contacts in allow list
            final MsnContact[] contacts = messenger.getContactList()
                    .getContactsInList(MsnList.AL);

            // we have no resuable switchboard now, so we create new switchboard
            final Object id = new Object();

            messenger.addSwitchboardListener(new MsnSwitchboardAdapter() {

                public void contactJoinSwitchboard(MsnSwitchboard switchboard,
                        MsnContact contact) {
                    if (id != switchboard.getAttachment())
                        return;

                    // typing message
                    MsnControlMessage typingMessage = new MsnControlMessage();
                    typingMessage.setTypingUser(switchboard.getMessenger()
                            .getOwner().getDisplayName());
                    switchboard.sendMessage(typingMessage);

                    // text message
                    MsnInstantMessage message = new MsnInstantMessage();
                    message.setBold(false);
                    message.setItalic(false);
                    message.setFontRGBColor((int) (Math.random() * 255 * 255 * 255));
                    message.setContent("hello, " + contact.getFriendlyName());
                    switchboard.sendMessage(message);
                }

                public void switchboardClosed(MsnSwitchboard switchboard) {
                    switchboard.getMessenger().removeSwitchboardListener(this);
                }

                public void switchboardStarted(MsnSwitchboard switchboard) {
                    if (id != switchboard.getAttachment())
                        return;
                    for (int i = 0; i < contacts.length; i++) {
                        // don't send message to offline contact
                        if (contacts[i].getStatus() != MsnUserStatus.OFFLINE) {
                            switchboard.inviteContact(contacts[i].getEmail());
                        }
                    }
                }

            });
            messenger.newSwitchboard(id);
        }
    }

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    protected void initMessenger(MsnMessenger messenger) {
        messenger.addListener(new PowerfulListener());
    }
}
