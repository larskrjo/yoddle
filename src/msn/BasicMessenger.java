package msn;

/*
 * Copyright 2004-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import net.sf.jml.MsnContact;
import net.sf.jml.MsnMessenger;
import net.sf.jml.MsnUserStatus;
import net.sf.jml.impl.MsnMessengerFactory;

/**
 * @author Roger Chen
 */
public class BasicMessenger extends JFrame {

    private static final long serialVersionUID = 1L;

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Usage: java messengerClassName email password");
            return;
        }
        BasicMessenger messenger = (BasicMessenger) Class.forName(args[0])
                .newInstance();
        messenger.setEmail(args[1]);
        messenger.setPassword(args[2]);
        messenger.start();
    }

    private String email;
    private String password;

    private MsnMessenger messenger;
    private JButton sendButton;

    private JTextField textField;

    private void createFrame() {

        textField = new JTextField(40);

        sendButton = new JButton("Send melding til alle på listen");
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < messenger.getContactList().getContacts().length; i++) {
                    MsnContact contact = messenger.getContactList()
                            .getContacts()[i];
                    // messenger.sendText(contact.getEmail(),
                    // textField.getText());
                    System.out.println(i + "" + contact.getEmail());
                }
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints g = new GridBagConstraints();
        g.fill = GridBagConstraints.BOTH;
        g.gridx = 0;
        g.gridy = 0;

        add(textField, g);

        g.gridy = 1;

        add(sendButton, g);

        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    protected void initMessenger(MsnMessenger messenger) {
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void start() {
        // create MsnMessenger instance
        messenger = MsnMessengerFactory.createMsnMessenger(email, password);

        // MsnMessenger support all protocols by default
        // messenger.setSupportedProtocol(new MsnProtocol[] { MsnProtocol.MSNP8
        // });

        // default init status is online,
        messenger.getOwner().setInitStatus(MsnUserStatus.ONLINE);

        // log incoming message
        messenger.setLogIncoming(true);

        // log outgoing message
        messenger.setLogOutgoing(true);

        initMessenger(messenger);
        messenger.login();

        createFrame();
    }
}
