package client.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import finalstatic.ColorPool;

/**
 * ClientController
 * 
 * @author Larsen, Håvard
 * 
 */
public class EmoticonPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private String basePath = "/images/emoticons/";
    private JButton smile = new JButton(new ImageIcon(getClass().getResource(
            basePath + "smile.png")));
    private JButton happy = new JButton(new ImageIcon(getClass().getResource(
            basePath + "happy.png")));
    private JButton surprised = new JButton(new ImageIcon(getClass()
            .getResource(basePath + "surprised.png")));
    private JButton tounge = new JButton(new ImageIcon(getClass().getResource(
            basePath + "tounge.png")));
    private JButton wink = new JButton(new ImageIcon(getClass().getResource(
            basePath + "wink.png")));
    private JButton sad = new JButton(new ImageIcon(getClass().getResource(
            basePath + "sad.png")));
    private JButton confused = new JButton(new ImageIcon(getClass()
            .getResource(basePath + "confused.png")));
    private JButton disappointed = new JButton(new ImageIcon(getClass()
            .getResource(basePath + "disappointed.png")));
    private JButton crying = new JButton(new ImageIcon(getClass().getResource(
            basePath + "crying.png")));
    private JButton embarrassed = new JButton(new ImageIcon(getClass()
            .getResource(basePath + "embarrassed.png")));
    private JButton hot = new JButton(new ImageIcon(getClass().getResource(
            basePath + "hot.png")));
    private JButton angry = new JButton(new ImageIcon(getClass().getResource(
            basePath + "angry.png")));
    private JButton fuckyou = new JButton(new ImageIcon(getClass().getResource(
            basePath + "fuckyou.png")));
    private JButton angel = new JButton(new ImageIcon(getClass().getResource(
            basePath + "angel.png")));
    private JButton contented = new JButton(new ImageIcon(getClass()
            .getResource(basePath + "contented.png")));
    /** Utility field used by event firing mechanism */
    private ArrayList<ActionListener> actionListenerList;

    public EmoticonPanel() {

        setLayout(new GridBagLayout());
        setBackground(ColorPool.LIGHTPURPLE);

        GridBagConstraints g = new GridBagConstraints();
        g.weightx = 0.0001;
        g.weighty = 1;
        g.anchor = GridBagConstraints.CENTER;
        g.fill = GridBagConstraints.VERTICAL;
        g.gridy = 0;

        smile.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":)", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 0;
        smile.setToolTipText("Type: :) or :-)");
        smile.setVisible(true);
        smile.setBorderPainted(false);
        smile.setContentAreaFilled(false);
        smile.setFocusable(false);
        add(smile, g);

        happy.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":D", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 1;
        happy.setToolTipText("Type: :D or :d");
        happy.setVisible(true);
        happy.setBorderPainted(false);
        happy.setContentAreaFilled(false);
        happy.setFocusable(false);
        add(happy, g);

        surprised.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":O", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 2;
        surprised.setToolTipText("Type: :-O, :o, :O or :-o");
        surprised.setVisible(true);
        surprised.setBorderPainted(false);
        surprised.setContentAreaFilled(false);
        surprised.setFocusable(false);
        add(surprised, g);

        tounge.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":P", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 3;
        tounge.setToolTipText("Type: :P, :p, :-P or :-p");
        tounge.setVisible(true);
        tounge.setBorderPainted(false);
        tounge.setContentAreaFilled(false);
        tounge.setFocusable(false);
        add(tounge, g);

        wink.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ";)", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 4;
        wink.setToolTipText("Type: ;) or ;-)");
        wink.setVisible(true);
        wink.setBorderPainted(false);
        wink.setContentAreaFilled(false);
        wink.setFocusable(false);
        add(wink, g);

        sad.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":(", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 5;
        sad.setToolTipText("Type: :( or :-(");
        sad.setVisible(true);
        sad.setBorderPainted(false);
        sad.setContentAreaFilled(false);
        sad.setFocusable(false);
        add(sad, g);

        confused.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":S", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 6;
        confused.setToolTipText("Type: :S, :s, :-S or :-s");
        confused.setVisible(true);
        confused.setBorderPainted(false);
        confused.setContentAreaFilled(false);
        confused.setFocusable(false);
        add(confused, g);

        disappointed.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":|", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 7;
        disappointed.setToolTipText("Type: :| or :-|");
        disappointed.setVisible(true);
        disappointed.setBorderPainted(false);
        disappointed.setContentAreaFilled(false);
        disappointed.setFocusable(false);
        add(disappointed, g);

        crying.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":'(", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 8;
        crying.setToolTipText("Type: :'(");
        crying.setVisible(true);
        crying.setBorderPainted(false);
        crying.setContentAreaFilled(false);
        crying.setFocusable(false);
        add(crying, g);

        embarrassed.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":$", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 9;
        embarrassed.setToolTipText("Type: :$ or :-$");
        embarrassed.setVisible(true);
        embarrassed.setBorderPainted(false);
        embarrassed.setContentAreaFilled(false);
        embarrassed.setFocusable(false);
        add(embarrassed, g);

        hot.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        "(H)", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 10;
        hot.setToolTipText("Type: (H) or (h)");
        hot.setVisible(true);
        hot.setBorderPainted(false);
        hot.setContentAreaFilled(false);
        hot.setFocusable(false);
        add(hot, g);

        angry.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ":@", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 11;
        angry.setToolTipText("Type: :@ or :-@");
        angry.setVisible(true);
        angry.setBorderPainted(false);
        angry.setContentAreaFilled(false);
        angry.setFocusable(false);
        add(angry, g);

        fuckyou.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        "(F)", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 12;
        fuckyou.setToolTipText("Type: (F) or (f)");
        fuckyou.setVisible(true);
        fuckyou.setBorderPainted(false);
        fuckyou.setContentAreaFilled(false);
        fuckyou.setFocusable(false);
        add(fuckyou, g);

        angel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        "(A)", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 13;
        angel.setToolTipText("Type: (A) or (a)");
        angel.setVisible(true);
        angel.setBorderPainted(false);
        angel.setContentAreaFilled(false);
        angel.setFocusable(false);
        add(angel, g);

        contented.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                processEvent(new ActionEvent(EmoticonPanel.this, e.getID(),
                        ";D", e.getWhen(), e.getModifiers()));
            }
        });
        g.gridx = 14;
        contented.setToolTipText("Type: ;D");
        contented.setVisible(true);
        contented.setBorderPainted(false);
        contented.setContentAreaFilled(false);
        contented.setFocusable(false);
        add(contented, g);
    }

    /** Register an action event listener */
    public void addActionListener(ActionListener l) {
        if (actionListenerList == null) {
            actionListenerList = new ArrayList<ActionListener>();
        }
        actionListenerList.add(l);
    }

    /**
     * prosessEvent distributes the events to its registered actionlisteners
     * 
     * @param e
     *            the ActionEvent from the source
     */
    private void processEvent(ActionEvent e) {
        for (int i = 0; i < actionListenerList.size(); i++) {
            actionListenerList.get(i).actionPerformed(e);
        }
    }

    /** Remove an action event listener */
    public void removeActionListener(ActionListener l) {
        if (actionListenerList != null && actionListenerList.contains(l)) {
            actionListenerList.remove(l);
        }
    }
}
