/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.text.Keymap;

/**
 * 
 * @author Håvard
 */
public class AdvancedTextArea extends JTextArea implements ActionListener {

    private static final long serialVersionUID = 1L;
    private Timer typingMonitor;
    private long lastInputTime;
    private long smileyDelayOffset;
    private boolean currentlyTyping;
    private boolean currentlyPickingSmiley;
    /** Utility field used by event firing mechanism */
    private ArrayList<ActionListener> actionListenerList;

    /**
     * Constructs an AdvancedTextArea object. This object is capable of
     * monitoring its own usage based on user interaction from the keyboard.
     * 
     * @param tickDelay
     *            how often the state variables is updated (time in
     *            milliseconds)
     * @param stopDelay
     *            specifies the time interval between typing and not typing
     *            (time in milliseconds)
     * @param smileyDelayOffset
     *            specifies the extra time between state changes given for
     *            interaction with the emoticonpanel. This can be enhance the
     *            user experience a little, since it usually takes some time
     *            going from a mouse click to the keyboard again. Provide a
     *            positive value to increase the delay. (time in milliseconds)
     */
    public AdvancedTextArea(final int tickDelay, final int stopDelay,
            final int smileyDelayOffset) {

        this.smileyDelayOffset = smileyDelayOffset;
        lastInputTime = 0; // A very long time ago
        currentlyTyping = false;
        currentlyPickingSmiley = false;

        performKeyboardCustomization();

        typingMonitor = initTypingMonitor(tickDelay, stopDelay);
        typingMonitor.start();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof EmoticonPanel) {
            lastInputTime = System.currentTimeMillis() + smileyDelayOffset;
            currentlyTyping = false;
            currentlyPickingSmiley = true;
            processEvent(new ActionEvent(AdvancedTextArea.this, e.getID(),
                    "selectedsmiley", e.getWhen(), e.getModifiers()));
            append(e.getActionCommand());
        }
    }

    /** Register an action event listener */
    public void addActionListener(ActionListener l) {
        if (actionListenerList == null) {
            actionListenerList = new ArrayList<ActionListener>();
        }
        actionListenerList.add(l);
    }

    /**
     * Used to create the instance of the typingMonitor whose target is to fire
     * certain events, and update state variables of the users acticity in the
     * <code>AdvancedTextArea</code>
     * 
     * @param tickDelay
     *            how often the state variables is updated (time in
     *            milliseconds)
     * @param stopDelay
     *            specifies the time interval between typing and not typing
     *            (time in milliseconds)
     * 
     * @return the newly constructed typingmonitor timer
     */
    private Timer initTypingMonitor(final int tickDelay, final int stopDelay) {

        Timer timer = new Timer(tickDelay, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if ((System.currentTimeMillis() - lastInputTime) >= stopDelay) {
                    if (currentlyTyping || currentlyPickingSmiley) {
                        if (getText().isEmpty()) {

                            processEvent(new ActionEvent(AdvancedTextArea.this,
                                    e.getID(), "removedtext", e.getWhen(),
                                    e.getModifiers()));
                            currentlyTyping = false;
                            currentlyPickingSmiley = false;
                        } else {
                            processEvent(new ActionEvent(AdvancedTextArea.this,
                                    e.getID(), "enteredtext", e.getWhen(),
                                    e.getModifiers()));
                            currentlyTyping = false;
                            currentlyPickingSmiley = false;
                        }
                    }
                }
            }
        });
        return timer;
    }

    private void performKeyboardCustomization() {
        // Customize the keymap
        Keymap keymap = getKeymap();
        keymap.addActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
                new AbstractAction() {

                    private static final long serialVersionUID = 1L;

                    public void actionPerformed(ActionEvent e) {
                        processEvent(new ActionEvent(AdvancedTextArea.this, e
                                .getID(), "send", e.getWhen(), e.getModifiers()));
                        processEvent(new ActionEvent(AdvancedTextArea.this, e
                                .getID(), "stoppedtyping", e.getWhen(), e
                                .getModifiers()));
                        currentlyTyping = false;
                        setText("");

                    }
                });

        keymap.addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,
                KeyEvent.SHIFT_DOWN_MASK), new AbstractAction() {

            private static final long serialVersionUID = 1L;

            public void actionPerformed(ActionEvent e) {
                int caret = getCaretPosition();
                String text = getText();
                setText(text.substring(0, caret) + "\n"
                        + text.substring(caret, text.length()));
                setCaretPosition(caret + 1);

            }
        });

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyTyped(KeyEvent e) {
                lastInputTime = System.currentTimeMillis();
                if (!currentlyTyping && (e.getKeyChar() != KeyEvent.VK_ENTER)) {
                    processEvent(new ActionEvent(AdvancedTextArea.this,
                            e.getID(), "istyping", e.getWhen(),
                            e.getModifiers()));
                    currentlyTyping = true;
                }
            }
        });
    }

    /**
     * prosessEvent distributes the events to its registered actionlisteners
     * 
     * @param e
     *            the ActionEvent from the source
     */
    private void processEvent(ActionEvent e) {
        for (int i = 0; i < actionListenerList.size(); i++) {
            actionListenerList.get(i).actionPerformed(e);
        }
    }

    /** Remove an action event listener */
    public void removeActionListener(ActionListener l) {
        if (actionListenerList != null && actionListenerList.contains(l)) {
            actionListenerList.remove(l);
        }
    }
}
