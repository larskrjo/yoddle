/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import protocol.Message;
import finalstatic.ColorPool;

/**
 * 
 * @author Håvard, Larsen
 */
public class LoginPanel extends JPanel {

    private static final long serialVersionUID = 1337L; // <- Lol!
    private JTextField username, serverip;
    private JLabel usernameLabel, serveripLabel;
    private JPanel usernamePanel, serveripPanel;
    private JButton signInButton;
    private URL imageURL;
    private ImageIcon imageLogo;
    private JLabel yoddle;

    // private EventListenerList ell;

    public LoginPanel(final ClientGUI clientGUI) {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(ColorPool.LIGHTBLUE); // kind of blue

        imageURL = getClass().getResource("/images/yoddleLogo.png");
        imageLogo = new ImageIcon(imageURL);
        yoddle = new JLabel(imageLogo);

        // Initialize components
        username = new JTextField(15);
        usernameLabel = new JLabel("Username:");
        usernameLabel.setBackground(ColorPool.LIGHTBLUE);
        usernameLabel.setOpaque(true);

        // Make the field slightly higher
        username.setPreferredSize(new Dimension(
                username.getPreferredSize().width,
                username.getPreferredSize().height + 5));
        // Håvard: 129.241.129.76
        // Larsen: 129.241.156.238
        serverip = new JTextField("localhost:55555");
        serveripLabel = new JLabel("Server IP:");
        serveripLabel.setBackground(ColorPool.LIGHTBLUE);
        serveripLabel.setOpaque(true);

        // Make the field slightly higher
        serverip.setPreferredSize(new Dimension(
                serverip.getPreferredSize().width,
                serverip.getPreferredSize().height + 5));

        usernamePanel = new JPanel(new GridLayout(2, 1, 0, 0));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(username);
        usernamePanel.setMaximumSize(new Dimension(150, 50));
        usernamePanel.setAlignmentX(CENTER_ALIGNMENT);

        serveripPanel = new JPanel(new GridLayout(2, 1, 0, 0));
        serveripPanel.add(serveripLabel);
        serveripPanel.add(serverip);
        serveripPanel.setMaximumSize(new Dimension(150, 50));
        serveripPanel.setAlignmentX(CENTER_ALIGNMENT);

        yoddle.setAlignmentX(CENTER_ALIGNMENT);

        signInButton = new JButton("Sign in");
        signInButton.setAlignmentX(CENTER_ALIGNMENT);

        add(yoddle);
        add(Box.createRigidArea(new Dimension(0, 20)));
        add(usernamePanel);
        add(Box.createRigidArea(new Dimension(0, 20)));
        add(serveripPanel);
        add(Box.createRigidArea(new Dimension(0, 20)));
        add(signInButton);

        signInButton.addActionListener(new ActionListener() {
            // Sends username to server

            public void actionPerformed(ActionEvent e) {
                // Ensure that username and server IP is set.
                if (!username.getText().equals("")
                        && !serverip.getText().equals("")) {

                    String[] string = serverip.getText().split(":");
                    // We demand port specification! Bwuhaha!
                    if (string.length == 2) {

                        try {
                            int port = Integer.valueOf(string[1]);
                            clientGUI.getClientController()
                                    .startClientListener(string[0], port);
                            clientGUI.getClientController().sendObject(
                                    new Message(Message.USERNAME, username
                                            .getText(), true, // isPut
                                            null, ColorPool.HTML_BLACK));
                        } catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog(clientGUI,
                                    "You have entered an invalid port number",
                                    "Port number error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }

            }
        });
    } // end of constructor

    public JButton getSignInButton() {
        return signInButton;
    }

    public JTextField getUsernameField() {
        return username;
    }
}
