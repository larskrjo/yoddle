package client.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;

import client.logic.ClientController;

import common.Misc;
import common.YoddleTray;

import finalstatic.ColorPool;

/**
 * ClientController
 * 
 * @author Larsen, Håvard (1337)
 * 
 */
public class ClientGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private ClientController clientController;
    private ChatPanel chatPanel;
    private LoginPanel loginPanel;

    private Font font;
    private String fontName;
    private String color;
    private ImageIcon trayImageIcon;
    private URL trayImageURL;

    public ClientGUI(ClientController clientController) {

        // Alert.AlertOnWindow(this);

        trayImageURL = getClass().getResource("/images/clientTrayIcon.png");
        trayImageIcon = new ImageIcon(trayImageURL);

        if (Misc.javaversion >= 6) {
            new YoddleTray(this, trayImageIcon, true);
        }

        setClientController(clientController);

        // Name the frame!
        setTitle("Yoddle");

        /*
         * Sets the icon for the JFrame. However, there seems to be a problem
         * with the alpha channel in the JFrame. The PNG image is not displayed
         * as a transparent image. :/
         */
        setIconImage(trayImageIcon.getImage());

        // Specify size of this frame
        setSize(300, 500);

        // Hides the windows from the screen
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        // Start the window at center of the screen
        setLocationRelativeTo(null);

        getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 0, 64));
        getContentPane().setBackground(ColorPool.LIGHTBLUE);

        chatPanel = new ChatPanel(this);
        loginPanel = new LoginPanel(this);

        /**
         * The clientController registers itself as a listener on the chatPanel
         * and the textinput area
         */
        chatPanel.addActionListener(clientController);
        chatPanel.getTextinput().addActionListener(clientController);

        /**
         * The textinput area registers itself as a listener on the
         * emoticonpanel
         */
        chatPanel.getEmoticonPanel()
                .addActionListener(chatPanel.getTextinput());

        // adds both chatpanel and loginpanel
        setChatPanel(chatPanel);
        setLoginPanel(loginPanel);

        // adds the loginpanel to the contentpane
        getContentPane().add(loginPanel);

        // set the default button for the frame
        getRootPane().setDefaultButton(loginPanel.getSignInButton());

        setVisible(true);

        // Makes the menubar active within the frame
        setJMenuBar(getTheMenuBar());

    } // End of constructor

    public ChatPanel getChatPanel() {
        return chatPanel;
    }

    public ClientController getClientController() {
        return clientController;
    }

    public String getColor() {
        return color;
    }

    public Font getFont() {
        return font;
    }

    public LoginPanel getLoginPanel() {
        return loginPanel;
    }

    private JMenu getSubColorMenu() {
        // Initial color of the text is black
        color = "black";
        // Creates the sub type font menu
        JMenu subColorMenu = new JMenu("Color");

        ButtonGroup buttonTypeGroup = new ButtonGroup();

        JRadioButtonMenuItem aqua = new JRadioButtonMenuItem("Aqua");
        JRadioButtonMenuItem black = new JRadioButtonMenuItem("Black");
        JRadioButtonMenuItem blue = new JRadioButtonMenuItem("Blue");
        JRadioButtonMenuItem fuchsia = new JRadioButtonMenuItem("Fuchsia");
        JRadioButtonMenuItem gray = new JRadioButtonMenuItem("Gray");
        JRadioButtonMenuItem green = new JRadioButtonMenuItem("Green");
        JRadioButtonMenuItem lime = new JRadioButtonMenuItem("Lime");
        JRadioButtonMenuItem maroon = new JRadioButtonMenuItem("Maroon");
        JRadioButtonMenuItem navy = new JRadioButtonMenuItem("Navy");
        JRadioButtonMenuItem red = new JRadioButtonMenuItem("Red");
        JRadioButtonMenuItem teal = new JRadioButtonMenuItem("Teal");
        JRadioButtonMenuItem white = new JRadioButtonMenuItem("White");
        JRadioButtonMenuItem yellow = new JRadioButtonMenuItem("Yellow");

        black.setSelected(true);

        buttonTypeGroup.add(aqua);
        buttonTypeGroup.add(black);
        buttonTypeGroup.add(blue);
        buttonTypeGroup.add(fuchsia);
        buttonTypeGroup.add(gray);
        buttonTypeGroup.add(green);
        buttonTypeGroup.add(lime);
        buttonTypeGroup.add(maroon);
        buttonTypeGroup.add(navy);
        buttonTypeGroup.add(red);
        buttonTypeGroup.add(teal);
        buttonTypeGroup.add(white);
        buttonTypeGroup.add(yellow);

        // Setting the different colors

        aqua.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "aqua";
                chatPanel.getTextinput().setForeground(new Color(0, 255, 255));
            }
        });
        black.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "black";
                chatPanel.getTextinput().setForeground(new Color(0, 0, 0));
            }
        });
        blue.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "blue";
                chatPanel.getTextinput().setForeground(new Color(0, 0, 255));
            }
        });
        fuchsia.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "fuchsia";
                chatPanel.getTextinput().setForeground(new Color(255, 0, 255));
            }
        });
        gray.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "gray";
                chatPanel.getTextinput()
                        .setForeground(new Color(128, 128, 128));
            }
        });
        green.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "green";
                chatPanel.getTextinput().setForeground(new Color(0, 128, 0));
            }
        });
        lime.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "lime";
                chatPanel.getTextinput().setForeground(new Color(0, 255, 0));
            }
        });
        maroon.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "maroon";
                chatPanel.getTextinput().setForeground(new Color(128, 0, 0));
            }
        });
        navy.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "navy";
                chatPanel.getTextinput().setForeground(new Color(0, 0, 128));
            }
        });
        red.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "red";
                chatPanel.getTextinput().setForeground(new Color(255, 0, 0));
            }
        });
        teal.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "teal";
                chatPanel.getTextinput().setForeground(new Color(0, 128, 128));
            }
        });
        white.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "white";
                chatPanel.getTextinput()
                        .setForeground(new Color(255, 255, 255));
            }
        });
        yellow.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                color = "yellow";
                chatPanel.getTextinput().setForeground(new Color(255, 255, 0));
            }
        });

        subColorMenu.add(aqua);
        subColorMenu.add(black);
        subColorMenu.add(blue);
        subColorMenu.add(fuchsia);
        subColorMenu.add(gray);
        subColorMenu.add(green);
        subColorMenu.add(lime);
        subColorMenu.add(maroon);
        subColorMenu.add(navy);
        subColorMenu.add(red);
        subColorMenu.add(teal);
        subColorMenu.add(white);
        subColorMenu.add(yellow);

        return subColorMenu;
    }

    private JMenuBar getTheMenuBar() {
        // Creates the menubar
        JMenuBar menuBar = new JMenuBar();

        // Creates the main menu
        JMenu menu = new JMenu("Menu");

        JMenuItem logOff = new JMenuItem("Log off");

        logOff.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {

                    public void run() {
                        // Log off
                    }
                });
            }
        });

        JMenuItem exit = new JMenuItem("Exit");

        exit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // Adding the items to the main menu
        menu.add(logOff);
        menu.add(exit);

        // Creates the font menu
        JMenu fontMenu = new JMenu("Font");

        // Creates the sub type font menu
        JMenu subTypeMenu = new JMenu("Type");

        ButtonGroup buttonTypeGroup = new ButtonGroup();

        JRadioButtonMenuItem timesNewRoman = new JRadioButtonMenuItem(
                "Times New Roman");
        JRadioButtonMenuItem comicSansMS = new JRadioButtonMenuItem(
                "Comic Sans MS");
        JRadioButtonMenuItem georgia = new JRadioButtonMenuItem("Georgia");
        timesNewRoman.setSelected(true);
        buttonTypeGroup.add(timesNewRoman);
        buttonTypeGroup.add(comicSansMS);
        buttonTypeGroup.add(georgia);

        // Creates the sub size font menu
        JMenu subSizeMenu = new JMenu("Size");

        ButtonGroup buttonSizeGroup = new ButtonGroup();

        JRadioButtonMenuItem smallSize = new JRadioButtonMenuItem("11");
        JRadioButtonMenuItem mediumSize = new JRadioButtonMenuItem("13");
        JRadioButtonMenuItem largeSize = new JRadioButtonMenuItem("15");
        mediumSize.setSelected(true);
        buttonSizeGroup.add(smallSize);
        buttonSizeGroup.add(mediumSize);
        buttonSizeGroup.add(largeSize);

        // Creates the sub style font menu
        JMenu subStyleMenu = new JMenu("Style");

        ButtonGroup buttonStyleGroup = new ButtonGroup();

        JRadioButtonMenuItem normal = new JRadioButtonMenuItem("Normal");
        JRadioButtonMenuItem italic = new JRadioButtonMenuItem("Italic");
        JRadioButtonMenuItem bold = new JRadioButtonMenuItem("Bold");
        normal.setSelected(true);
        buttonStyleGroup.add(normal);
        buttonStyleGroup.add(italic);
        buttonStyleGroup.add(bold);

        // Setting the standard font

        font = new Font("Times New Roman", Font.PLAIN, 13);

        fontName = "Times New Roman";

        // Setting the Times New Roman font
        timesNewRoman.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fontName = "Times New Roman";
                font = new Font(fontName, font.getStyle(), font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the Comic Sans MS font
        comicSansMS.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fontName = "Comic Sans MS";
                font = new Font(fontName, font.getStyle(), font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the Serif font
        georgia.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fontName = "Georgia";
                font = new Font(fontName, font.getStyle(), font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the fontsize to 11
        smallSize.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, font.getStyle(), 11);
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the fontsize to 13
        mediumSize.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, font.getStyle(), 13);
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the fontsize to 15
        largeSize.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, font.getStyle(), 15);
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the font to normal style
        normal.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, Font.PLAIN, font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the font to italic style
        italic.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, Font.ITALIC, font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Setting the font to bold style
        bold.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                font = new Font(fontName, Font.BOLD, font.getSize());
                chatPanel.getTextinput().setFont(font);
            }
        });

        // Adds type fonts
        subTypeMenu.add(timesNewRoman);
        subTypeMenu.add(comicSansMS);
        subTypeMenu.add(georgia);

        // Adds size fonts
        subSizeMenu.add(smallSize);
        subSizeMenu.add(mediumSize);
        subSizeMenu.add(largeSize);

        // Adds style fonts
        subStyleMenu.add(normal);
        subStyleMenu.add(italic);
        subStyleMenu.add(bold);

        // Adds the submenus
        fontMenu.add(subTypeMenu);
        fontMenu.add(subSizeMenu);
        fontMenu.add(subStyleMenu);
        fontMenu.add(getSubColorMenu());

        // Adds the menus to the menubar
        menuBar.add(menu);
        menuBar.add(fontMenu);

        return menuBar;
    }

    public void setChatPanel(ChatPanel chatPanel) {
        this.chatPanel = chatPanel;
    }

    public void setClientController(ClientController clientController) {
        this.clientController = clientController;
    }

    public void setLoginPanel(LoginPanel loginPanel) {
        this.loginPanel = loginPanel;
    }

    /**
     * Changes from loginscreen to chatscreen
     */
    public void switchScreen() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                getRootPane().setDefaultButton(null); // remove default button
                getContentPane().remove(getLoginPanel());
                setContentPane(getChatPanel());
                setSize(600, 500);
                chatPanel.getTextinput().requestFocusInWindow();
                validate();
            }
        });
    }
}
