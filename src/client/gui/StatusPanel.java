/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;

import finalstatic.ColorPool;

/**
 * 
 * @author Håvard
 */
public class StatusPanel extends JPanel {

    private class UserState {

        private String username;
        private String state;

        private UserState(String username, String state) {
            this.username = username;
            this.state = state;
        }
    }

    private static final long serialVersionUID = 31415L;
    Vector<UserState> userstates;
    JLabel statesLabel;

    GridBagConstraints g;

    /** Utility field used by event firing mechanism */
    private ArrayList<ActionListener> actionListenerList;

    public StatusPanel() {

        userstates = new Vector<UserState>();

        setLayout(new GridBagLayout());
        setBackground(ColorPool.LIGHTPURPLE);

        statesLabel = new JLabel(" ");
        statesLabel.setMinimumSize(new Dimension(0, statesLabel
                .getPreferredSize().height + 15));

        g = new GridBagConstraints(); // init or reset constraints
        g.gridx = 0; // Leftmost column
        g.gridy = 0; // First row
        g.weightx = 1; // Sets the intial weight in x-direction to 1
        g.weighty = 0; // Sets the intial weight in y-direction to 1
        g.fill = GridBagConstraints.BOTH; // Make the window being filled with
        // content in both x and y directions

        add(statesLabel, g);
    }

    /** Register an action event listener */
    public void addActionListener(ActionListener l) {
        if (actionListenerList == null) {
            actionListenerList = new ArrayList<ActionListener>();
        }
        actionListenerList.add(l);
    }

    /** Remove an action event listener */
    public void removeActionListener(ActionListener l) {
        if (actionListenerList != null && actionListenerList.contains(l)) {
            actionListenerList.remove(l);
        }
    }

    /**
     * prosessEvent distributes the events to its registered actionlisteners
     * 
     * @param e
     *            the ActionEvent from the source
     */
    // private void processEvent(ActionEvent e) {
    // for (int i = 0; i < actionListenerList.size(); i++) {
    // actionListenerList.get(i).actionPerformed(e);
    // }
    // }

    /**
     * Adds the userstate if the username is not already registered
     */
    public void setUserState(String username, String state) {
        int index = -1;
        for (int i = 0; i < userstates.size(); i++) {
            if (userstates.get(i).username.equals(username)) {
                index = i;
                break;
            }
        }
        if (index < 0) { // not registered
            userstates.add(new UserState(username, state));
            updateStatesLabel();
        } else { // already registered
            if (state == null) {
                userstates.remove(index);
                updateStatesLabel();
            } else {
                userstates.get(index).state = state;
                updateStatesLabel();
            }
        }
    }

    private void updateStatesLabel() {
        String content = "";
        for (UserState user : userstates) {
            if (user.state != null) {
                content += user.username + " " + user.state + " ";
            }
        }
        statesLabel.setText(content);
    }
}
