/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;

import finalstatic.CSS;
import finalstatic.ColorPool;

/**
 * 
 * @author Håvard, Larsen
 */
public class ChatPanel extends JPanel {

    private static final long serialVersionUID = 2718281828459045L;
    /**
     * Gui spesific variables
     */
    private JEditorPane conversation;
    private JEditorPane userbar;
    private AdvancedTextArea textinput;
    private JScrollPane scrollConv, scrollUser;
    private JSplitPane splitPane;
    private GridBagConstraints g;
    // private ClientGUI clientGUI;
    private String statusText = "<u><b>Online Users:</b></u>";
    private String welcomeText = "<h1 id=\"welcome\">Welcome to Yoddle!</p>";
    private StatusPanel statusPanel;
    private EmoticonPanel emoticonPanel;
    /** Utility field used by event firing mechanism */
    private ArrayList<ActionListener> actionListenerList;

    /**
     * Creates the panel where you chat
     * 
     * @param clientGUI
     *            is the chatpanel
     */
    public ChatPanel(ClientGUI clientGUI) {

        // this.clientGUI = clientGUI;

        setLayout(new GridBagLayout());
        setBackground(ColorPool.BLUEGREY);

        // Initialize components
        conversation = new JEditorPane();
        conversation.setContentType("text/html");
        conversation.setEditable(false);
        conversation.setBackground(ColorPool.LIGHTBLUE);
        conversation.setText(welcomeText);

        userbar = new JEditorPane();
        userbar.setContentType("text/html");
        userbar.setText(statusText);
        userbar.setEditable(false);
        userbar.setBackground(ColorPool.LIGHTPURPLE);

        statusPanel = new StatusPanel();
        emoticonPanel = new EmoticonPanel();

        HTMLDocument doc1 = (HTMLDocument) conversation.getDocument();
        HTMLDocument doc2 = (HTMLDocument) userbar.getDocument();
        Element header1 = doc1.getDefaultRootElement().getElement(0);
        Element header2 = doc2.getDefaultRootElement().getElement(0);
        try {
            doc1.insertBeforeEnd(header1, CSS.chatRoom);
            doc2.insertBeforeEnd(header2, CSS.statusbar);

        } catch (BadLocationException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        textinput = new AdvancedTextArea(50, // tick delay (ms)
                500, // stop delay (ms)
                500); // smiley delay offset (ms)

        textinput.setBorder(BorderFactory.createBevelBorder(
                BevelBorder.LOWERED, new Color(162, 162, 162), // Highlight
                // colour
                new Color(162, 162, 162) // Shadow colour
                ));
        textinput.setFont(clientGUI.getFont());

        textinput.setLineWrap(true);

        scrollConv = new JScrollPane(conversation,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollConv.setMinimumSize(new Dimension(400, 200));

        scrollUser = new JScrollPane(userbar,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        scrollUser.setMinimumSize(new Dimension(100, 200));

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollConv,
                scrollUser);
        splitPane.setMinimumSize(new Dimension(500, 200));
        splitPane.setResizeWeight(0.95);

        // Positioning components
        g = new GridBagConstraints(); // init or reset constraints
        g.gridx = 0; // Leftmost column
        g.gridy = 0; // First row
        g.weightx = 1; // Sets the intial weight in x-direction to 1
        g.weighty = 1; // Sets the intial weight in y-direction to 1
        g.fill = GridBagConstraints.BOTH; // Make the window being filled with
        // content in both x and y directions

        add(splitPane, g);

        g.gridx = 0; // Leftmost column
        g.gridy = 1; // Last row
        g.weightx = 1; // Gets all horizontal space
        g.weighty = 0; // Gets NO vertical space;
        g.fill = GridBagConstraints.BOTH;
        g.insets = new Insets(5, 5, 5, 5);

        add(statusPanel, g);

        g.gridx = 0; // Leftmost column
        g.gridy = 2; // Last row
        g.weightx = 1; // Gets all horizontal space
        g.weighty = 0; // Gets NO vertical space;
        g.fill = GridBagConstraints.BOTH;
        g.insets = new Insets(5, 5, 5, 5);

        add(textinput, g);

        g.gridx = 0; // Leftmost column
        g.gridy = 3; // Last row
        g.weightx = 1; // Gets all horizontal space
        g.weighty = 0; // Gets NO vertical space;
        g.fill = GridBagConstraints.BOTH;
        g.insets = new Insets(0, 5, 5, 5);

        add(emoticonPanel, g);

    } // end of constructor

    /** Register an action event listener */
    public void addActionListener(ActionListener l) {
        if (actionListenerList == null) {
            actionListenerList = new ArrayList<ActionListener>();
        }
        actionListenerList.add(l);
    }

    /**
     * Appends text to the chat area.
     * 
     * @param message
     *            the message to be appended
     */
    public void appendToChat(final String message) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                // Alert.alerter.flash(clientGUI);
                try {
                    HTMLDocument doc = (HTMLDocument) conversation
                            .getDocument();
                    Element root = doc.getDefaultRootElement().getElement(1);
                    doc.setBase(ChatPanel.class
                            .getResource("/images/emoticons/"));
                    String s = getSmileys(message);
                    doc.insertBeforeEnd(root, s);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                conversation.setCaretPosition(conversation.getDocument()
                        .getLength());
            }
        });
    }

    /**
     * Appends text to the user area.
     * 
     * @param text
     *            the text to be appended
     */
    public void appendToUserBar(final String text) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                try {
                    HTMLDocument doc = (HTMLDocument) userbar.getDocument();

                    Element body = doc.getDefaultRootElement().getElement(1);
                    doc.insertBeforeEnd(body, "<p>" + text + "</p>");
                    userbar.setCaretPosition(userbar.getDocument().getLength());

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    /**
     * Clears the text within the text input field.
     */
    public void clearMessage() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                textinput.setText("");
            }
        });
    }

    /**
     * Clears the text within the user field.
     */
    public void clearUsers() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                userbar.setText(statusText);
            }
        });
    }

    /**
     * prosessEvent distributes the events to its registered actionlisteners
     * 
     * @param e
     *            the ActionEvent from the source
     */
    // private void processEvent(ActionEvent e) {
    // for (int i = 0; i < actionListenerList.size(); i++) {
    // actionListenerList.get(i).actionPerformed(e);
    // }
    // }

    public JEditorPane getConversation() {
        return conversation;
    }

    public EmoticonPanel getEmoticonPanel() {
        return emoticonPanel;
    }

    /**
     * Returns the text currently contained within the text input field.
     * 
     * @return the text of the text input field.
     */
    public String getMessage() {
        return textinput.getText();
    }

    private String getSmileys(String message) {
        // Checks for different emoticons
        String result = "";
        result = message.replaceAll(":\\)", "<img src=\"smile.png\">");
        message = result;
        result = message.replaceAll(":-\\)", "<img src=\"smile.png\">");
        message = result;
        result = message.replaceAll(":D", "<img src=\"happy.png\">");
        message = result;
        result = message.replaceAll(":d", "<img src=\"happy.png\">");
        message = result;
        result = message.replaceAll(":-O", "<img src=\"surprised.png\">");
        message = result;
        result = message.replaceAll(":o", "<img src=\"surprised.png\">");
        message = result;
        result = message.replaceAll(":O", "<img src=\"surprised.png\">");
        message = result;
        result = message.replaceAll(":-o", "<img src=\"surprised.png\">");
        message = result;
        result = message.replaceAll(":-P", "<img src=\"tounge.png\">");
        message = result;
        result = message.replaceAll(":P", "<img src=\"tounge.png\">");
        message = result;
        result = message.replaceAll(":p", "<img src=\"tounge.png\">");
        message = result;
        result = message.replaceAll(":-p", "<img src=\"tounge.png\">");
        message = result;
        result = message.replaceAll(";\\)", "<img src=\"wink.png\">");
        message = result;
        result = message.replaceAll(";-\\)", "<img src=\"wink.png\">");
        message = result;
        result = message.replaceAll(":\\(", "<img src=\"sad.png\">");
        message = result;
        result = message.replaceAll(":-\\(", "<img src=\"sad.png\">");
        message = result;
        result = message.replaceAll(":s", "<img src=\"confused.png\">");
        message = result;
        result = message.replaceAll(":-S", "<img src=\"confused.png\">");
        message = result;
        result = message.replaceAll(":-s", "<img src=\"confused.png\">");
        message = result;
        result = message.replaceAll(":S", "<img src=\"confused.png\">");
        message = result;
        result = message.replaceAll(":-\\|", "<img src=\"disappointed.png\">");
        message = result;
        result = message.replaceAll(":\\|", "<img src=\"disappointed.png\">");
        message = result;
        result = message.replaceAll(":'\\(", "<img src=\"crying.png\">");
        message = result;
        result = message.replaceAll(":-\\$", "<img src=\"embarrassed.png\">");
        message = result;
        result = message.replaceAll(":\\$", "<img src=\"embarrassed.png\">");
        message = result;
        result = message.replaceAll("\\(h\\)", "<img src=\"hot.png\">");
        message = result;
        result = message.replaceAll("\\(H\\)", "<img src=\"hot.png\">");
        message = result;
        result = message.replaceAll(":-@", "<img src=\"angry.png\">");
        message = result;
        result = message.replaceAll(":@", "<img src=\"angry.png\">");
        message = result;
        result = message.replaceAll("\\(f\\)", "<img src=\"fuckyou.png\">");
        message = result;
        result = message.replaceAll("\\(F\\)", "<img src=\"fuckyou.png\">");
        message = result;
        result = message.replaceAll("\\(a\\)", "<img src=\"angel.png\">");
        message = result;
        result = message.replaceAll("\\(A\\)", "<img src=\"angel.png\">");
        message = result;
        result = message.replaceAll(";D", "<img src=\"contented.png\">");
        message = result;

        return result;
    }

    public StatusPanel getStatusPanel() {
        return statusPanel;
    }

    public AdvancedTextArea getTextinput() {
        return textinput;
    }

    /** Remove an action event listener */
    public void removeActionListener(ActionListener l) {
        if (actionListenerList != null && actionListenerList.contains(l)) {
            actionListenerList.remove(l);
        }
    }
}
