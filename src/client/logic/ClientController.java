package client.logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import javax.swing.SwingUtilities;

import protocol.List;
import protocol.Message;
import protocol.Notification;
import protocol.Packet;
import client.gui.AdvancedTextArea;
import client.gui.ClientGUI;
import client.listener.ClientListener;
import finalstatic.StringReplacers;

/**
 * ClientController
 * 
 * @author Larsen, Håvard
 * 
 */
public class ClientController implements ActionListener {

    private ClientListener clientListener;
    private ClientGUI clientGUI;

    public ClientController() {

        final ClientController clientController = this;

        /**
         * Invokes the GUI
         */
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                public void run() {
                    setClientGUI(new ClientGUI(clientController));
                }
            });
        } catch (InterruptedException e) {
            System.out
                    .println("Something went wrong initilalizing the server GUI. ");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out
                    .println("Something went wrong initilalizing the server GUI.");
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof AdvancedTextArea) {
            if (e.getActionCommand().equals("send")) {
                sendObject(new Message(Message.TEXT,
                        ((AdvancedTextArea) e.getSource()).getText().replace(
                                "\n", StringReplacers.NEWLINE), true,
                        clientGUI.getFont(), clientGUI.getColor()));
            } else if (e.getActionCommand().equals("istyping")) {
                sendObject(new Notification(Notification.ISTYPING, true, null));
            } else if (e.getActionCommand().equals("stoppedtyping")) {
                sendObject(new Notification(Notification.STOPPEDTYPING, true,
                        null));
            } else if (e.getActionCommand().equals("enteredtext")) {
                sendObject(new Notification(Notification.ENTEREDTEXT, true,
                        null));
            } else if (e.getActionCommand().equals("removedtext")) {
                sendObject(new Notification(Notification.REMOVEDTEXT, true,
                        null));
            } else if (e.getActionCommand().equals("selectedsmiley")) {
                sendObject(new Notification(Notification.SELECTEDSMILEY, true,
                        null));
            }
        } else {
        }

    }

    public ClientGUI getClientGUI() {
        return clientGUI;
    }

    /**
     * Receives objects from the listener, usually Messages, and allways HGLJ.
     * 
     * @param o
     *            the object which comes from the server
     */
    public void receiveObject(Object o) {
        Packet packet = (Packet) o;
        if (packet.isPut()) { // SEND type
            if (packet instanceof Message) {
                /*
                 * This is an instance of the message class. We give it a
                 * Message pointer to avoid further type casting.
                 */
                Message msg = (Message) packet;

                if (msg.getType() == Message.TEXT) {
                    clientGUI.getChatPanel().appendToChat((msg.getMessage()));
                } else if (msg.getType() == Message.LOGGEDIN) {
                    clientGUI.switchScreen();
                }
            } else if (packet instanceof List) {
                /*
                 * This is an instance of the list class. We give it a List
                 * pointer to avoid further type casting.
                 */
                List list = (List) packet;

                if (packet.getType() == List.ONLINEUSERS) {
                    updateBuddyList((Vector<Object>) list.getObjects());
                }
            } else if (packet instanceof Notification) {
                /*
                 * This is an instance of the notification class. We give it a
                 * Notification pointer to avoid further type casting.
                 */
                Notification notif = (Notification) packet;

                if (notif.getType() == Notification.ISTYPING) {
                    getClientGUI().getChatPanel().getStatusPanel()
                            .setUserState(notif.getSourceUser(), "is typing.");
                } else if (notif.getType() == Notification.STOPPEDTYPING
                        || notif.getType() == Notification.REMOVEDTEXT) {
                    getClientGUI().getChatPanel().getStatusPanel()
                            .setUserState(notif.getSourceUser(), null);
                } else if (notif.getType() == Notification.ENTEREDTEXT) {
                    getClientGUI()
                            .getChatPanel()
                            .getStatusPanel()
                            .setUserState(notif.getSourceUser(),
                                    "has entered text.");
                } else if (notif.getType() == Notification.SELECTEDSMILEY) {
                    getClientGUI()
                            .getChatPanel()
                            .getStatusPanel()
                            .setUserState(notif.getSourceUser(),
                                    "has picked smiley.");
                }
            }
        } else { // GET type
        }
    }

    /**
     * Sends the passed object to the server
     * 
     * @param o
     *            is the passed object
     */
    public void sendObject(Object o) {
        clientListener.sendObject(o);
    }

    public void setClientGUI(ClientGUI clientGUI) {
        this.clientGUI = clientGUI;
    }

    /**
     * This method is called by the loginpanel, where it initializes the
     * Listener to the server
     * 
     * @param ip
     *            the ip to the server
     * @param port
     *            the port to the server
     */
    public void startClientListener(String ip, int port) {
        new Thread(clientListener = new ClientListener(this, ip, port)).start();
    }

    /**
     * This is called from the server, and updates the userbar
     * 
     * @param buddyList
     */
    private void updateBuddyList(Vector<Object> buddyList) {
        clientGUI.getChatPanel().clearUsers();
        for (int i = 0; i < buddyList.size(); i++) {
            System.out.println((String) buddyList.get(i));
            clientGUI.getChatPanel().appendToUserBar((String) buddyList.get(i));
        }
    }
}
