package client.logic;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MainClient {

    /**
     * Starts the application
     * 
     * @param args
     *            null
     */
    public static void main(String[] args) {

        // Sets the look and feel to OS spesific
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (InstantiationException e1) {
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        } catch (UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }
        new ClientController();
    }
}
