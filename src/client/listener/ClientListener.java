package client.listener;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import client.logic.ClientController;

public class ClientListener implements Runnable {

    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private ClientController clientController;
    private Socket socket;
    private Object o;

    public ClientListener(ClientController clientController, String ip, int port) {

        this.clientController = clientController;
        /**
         * Creates the input and output connections
         */
        try {
            socket = new Socket(ip, port);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Listening for objects from server and passes them to clientController
     */
    private void receiveObject() {
        try {
            while (true) {
                o = inputStream.readObject();
                clientController.receiveObject(o);
            }
        } catch (IOException e) {
            System.out.println("Lost connection with server");
            // e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void run() {
        // Start listening from server
        receiveObject();
    }

    /**
     * Passes the objects from clientController to server
     * 
     * @param o
     *            is the object who is passed to the server
     */
    public synchronized void sendObject(Object o) {
        try {
            outputStream.writeObject(o);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
