package server.listener;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import protocol.Message;
import server.logic.UserController;
import finalstatic.Broadcast;
import finalstatic.ColorPool;

/**
 * UserListener listens for input and output to the clients
 * 
 * @author Larsen, Håvard
 * 
 */
public class UserListener implements Runnable {

    /**
     * The different streams
     */
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private UserController userController;
    /**
     * The object which gonna be passed
     */
    private Object o;

    /**
     * Initializes the connection to the client
     * 
     * @param userController
     *            is the creator of this object
     * @param socket
     *            is the connection socket where the client and server
     *            communicates
     */
    public UserListener(UserController userController, Socket socket) {
        this.userController = userController;

        /**
         * Initializes the streams
         */
        try {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Listens for objects from the client, and passes it to the
     * userController's receiveObject metod
     */
    private void receiveObject() {
        try {
            while (true) {
                o = inputStream.readObject();
                userController.receiveObject(o);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("User disconnected");
            // e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            /**
             * After a session the streams must be closed and the user removed
             * from the vector
             */
            try {
                inputStream.close();
                outputStream.close();
                userController.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            userController.getServerController().removeUserController(
                    userController);
            userController.getServerController().updateBuddyList();
            userController.broadcast(new Message(Message.TEXT,
                    "<div id=\"offline\">*** User " + userController.getName()
                            + " is offline ***</div>", true, null,
                    ColorPool.HTML_RED), Broadcast.ALLBUTME);
        }
    }

    /**
     * Is called after the object is created. It starts to listen for objects
     * from the client
     */
    public void run() {
        receiveObject();
    }

    /**
     * It passes the object to the client
     * 
     * @param o
     *            the object which gonna be passed
     */
    public void sendObject(Object o) {
        try {
            outputStream.writeObject(o);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
