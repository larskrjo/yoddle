package server.logic;

import java.awt.Font;
import java.net.Socket;
import java.util.Vector;

import protocol.List;
import protocol.Message;
import protocol.Notification;
import protocol.Packet;
import server.listener.UserListener;
import finalstatic.Broadcast;
import finalstatic.ColorPool;
import finalstatic.StringReplacers;

/**
 * The UserController communicates with UserListener, where it sends and
 * receives objects. It also communicates with the ServerController for common
 * info about the users. There is one UserController pr. user, and only one
 * ServerController
 * 
 * @author Larsen, Håvard
 * 
 */
public class UserController {

    public static final String escapeHTML(Message s) {
        StringBuffer sb = new StringBuffer();
        int n = s.getMessage().length();
        for (int i = 0; i < n; i++) {
            char c = s.getMessage().charAt(i);
            // Recognize the hidden word for new line
            if (i + 19 < n
                    && s.getMessage().charAt(i) == StringReplacers.NEWLINE
                            .charAt(0)
                    && s.getMessage().charAt(i + 1) == StringReplacers.NEWLINE
                            .charAt(1)
                    && s.getMessage().charAt(i + 2) == StringReplacers.NEWLINE
                            .charAt(2)
                    && +s.getMessage().charAt(i + 3) == StringReplacers.NEWLINE
                            .charAt(3)
                    && s.getMessage().charAt(i + 4) == StringReplacers.NEWLINE
                            .charAt(4)
                    && s.getMessage().charAt(i + 5) == StringReplacers.NEWLINE
                            .charAt(5)
                    && +s.getMessage().charAt(i + 6) == StringReplacers.NEWLINE
                            .charAt(6)
                    && s.getMessage().charAt(i + 7) == StringReplacers.NEWLINE
                            .charAt(7)
                    && s.getMessage().charAt(i + 8) == StringReplacers.NEWLINE
                            .charAt(8)
                    && +s.getMessage().charAt(i + 9) == StringReplacers.NEWLINE
                            .charAt(9)
                    && s.getMessage().charAt(i + 10) == StringReplacers.NEWLINE
                            .charAt(10)
                    && s.getMessage().charAt(i + 11) == StringReplacers.NEWLINE
                            .charAt(11)
                    && +s.getMessage().charAt(i + 12) == StringReplacers.NEWLINE
                            .charAt(12)
                    && s.getMessage().charAt(i + 13) == StringReplacers.NEWLINE
                            .charAt(13)
                    && s.getMessage().charAt(i + 14) == StringReplacers.NEWLINE
                            .charAt(14)
                    && +s.getMessage().charAt(i + 15) == StringReplacers.NEWLINE
                            .charAt(15)
                    && s.getMessage().charAt(i + 16) == StringReplacers.NEWLINE
                            .charAt(16)
                    && s.getMessage().charAt(i + 17) == StringReplacers.NEWLINE
                            .charAt(17)
                    && +s.getMessage().charAt(i + 18) == StringReplacers.NEWLINE
                            .charAt(18)
                    && s.getMessage().charAt(i + 19) == StringReplacers.NEWLINE
                            .charAt(19)) {

                i = i + 19;

                // Haxx to make copy and paste avaliable more than once in chat
                String fontStyle = "";
                switch (s.getFont().getStyle()) {
                case 0:
                    fontStyle = "normal";
                    break;
                case 1:
                    fontStyle = "bold";
                    break;
                case 2:
                    fontStyle = "italic";
                    break;
                }
                sb.append("</div>" + "<div id=\"message\" style=\"" + "color:"
                        + s.getColor() + ";" + "font-family:"
                        + s.getFont().getFontName() + ";font-style:"
                        + fontStyle + ";font-size:" + s.getFont().getSize()
                        + "\">");
                continue;
            }

            if (i + 6 < n && s.getMessage().charAt(i) == HTMLPROTOCOL.charAt(0)
                    && s.getMessage().charAt(i + 1) == HTMLPROTOCOL.charAt(1)
                    && s.getMessage().charAt(i + 2) == HTMLPROTOCOL.charAt(2)
                    && +s.getMessage().charAt(i + 3) == HTMLPROTOCOL.charAt(3)
                    && s.getMessage().charAt(i + 4) == HTMLPROTOCOL.charAt(4)
                    && s.getMessage().charAt(i + 5) == HTMLPROTOCOL.charAt(5)
                    && +s.getMessage().charAt(i + 6) == HTMLPROTOCOL.charAt(6)) {
                sb.append("<a href=\"");
                for (int j = i; j < n; j++) {
                    if (s.getMessage().charAt(j) == ' ') {
                        sb.append("\">" + s.getMessage().substring(i, j)
                                + "</a> ");
                        i = j;
                        break;
                    }
                    sb.append(s.getMessage().charAt(j));
                }
                continue;
            }

            // Haxx to make line wrapping functional
            if (i + 1 < n && s.getMessage().charAt(i) == ' '
                    && s.getMessage().charAt(i + 1) != ' ') {
                sb.append(" ");
                continue;
            }

            switch (c) {
            case '<':
                sb.append("&lt;");
                break;
            case '>':
                sb.append("&gt;");
                break;
            case '&':
                sb.append("&amp;");
                break;
            case '"':
                sb.append("&quot;");
                break;
            // case 'Ã ': sb.append("&agrave;");break;
            // case 'Ã€': sb.append("&Agrave;");break;
            // case 'Ã¢': sb.append("&acirc;");break;
            // case 'Ã‚': sb.append("&Acirc;");break;
            // case 'Ã¤': sb.append("&auml;");break;
            // case 'Ã„': sb.append("&Auml;");break;
            // case 'Ã¥': sb.append("&aring;");break;
            // case 'Ã…': sb.append("&Aring;");break;
            // case 'Ã¦': sb.append("&aelig;");break;
            // case 'Ã†': sb.append("&AElig;");break;
            // case 'Ã§': sb.append("&ccedil;");break;
            // case 'Ã‡': sb.append("&Ccedil;");break;
            // case 'Ã©': sb.append("&eacute;");break;
            // case 'Ã‰': sb.append("&Eacute;");break;
            // case 'Ã¨': sb.append("&egrave;");break;
            // case 'Ãˆ': sb.append("&Egrave;");break;
            // case 'Ãª': sb.append("&ecirc;");break;
            // case 'ÃŠ': sb.append("&Ecirc;");break;
            // case 'Ã«': sb.append("&euml;");break;
            // case 'Ã‹': sb.append("&Euml;");break;
            // case 'Ã¯': sb.append("&iuml;");break;
            // case 'Ã�': sb.append("&Iuml;");break;
            // case 'Ã´': sb.append("&ocirc;");break;
            // case 'Ã”': sb.append("&Ocirc;");break;
            // case 'Ã¶': sb.append("&ouml;");break;
            // case 'Ã–': sb.append("&Ouml;");break;
            // case 'Ã¸': sb.append("&oslash;");break;
            // case 'Ã˜': sb.append("&Oslash;");break;
            // case 'ÃŸ': sb.append("&szlig;");break;
            // case 'Ã¹': sb.append("&ugrave;");break;
            // case 'Ã™': sb.append("&Ugrave;");break;
            // case 'Ã»': sb.append("&ucirc;");break;
            // case 'Ã›': sb.append("&Ucirc;");break;
            // case 'Ã¼': sb.append("&uuml;");break;
            // case 'Ãœ': sb.append("&Uuml;");break;
            // case 'Â®': sb.append("&reg;");break;
            // case 'Â©': sb.append("&copy;");break;
            // case 'â‚¬': sb.append("&euro;"); break;
            // be carefull with this one (non-breaking whitee space)
            case ' ':
                sb.append("&nbsp;");
                break;

            default:
                sb.append(c);
                break;
            }
        }
        return sb.toString();
    }

    private ServerController serverController;
    private UserListener userListener;
    private Socket socket;
    private String name = "UNDEFINED";

    /**
     * Did this client send the last message?
     */
    private boolean spokeLast = false;

    private static final String HTMLPROTOCOL = "http://";

    /**
     * This starts the thread where the connection between the client and the
     * server
     * 
     * @param socket
     *            the socket to communicate with the client
     * @param serverController
     */
    public UserController(Socket socket, ServerController serverController) {

        this.serverController = serverController;
        this.socket = socket;
        new Thread(userListener = new UserListener(this, socket)).start();

    }

    /**
     * If this user spoke last, the message will only contain the message
     * itself. If this is not the case, we append the username to the message.
     * In this last case we also need to update who spoke last.
     */
    private Message addNameToMessage(Message message) {
        /*
         * Build the appropriate message string
         */
        String fontStyle = "";
        switch (message.getFont().getStyle()) {
        case 0:
            fontStyle = "normal";
            break;
        case 1:
            fontStyle = "bold";
            break;
        case 2:
            fontStyle = "italic";
            break;
        }
        // Overrides the style in the div-tag.
        String messageContent = (spokeLast) ? "<div id=\"message\" style=\""
                + "color:" + message.getColor() + ";" + "font-family:"
                + message.getFont().getFontName() + ";font-style:" + fontStyle
                + ";font-size:" + message.getFont().getSize() + "\">"
                + escapeHTML(message) + "</div>" : "<div id=\"nickname\">"
                + name + " says:</div>" + "<div id=\"message\" style=\""
                + "color:" + message.getColor() + ";" + "font-family:"
                + message.getFont().getFontName() + ";font-style:" + fontStyle
                + ";font-size:" + message.getFont().getSize() + "\">"
                + escapeHTML(message) + "</div>";
        /*
         * This user has now become the last speaker, thus we update who spoke
         * last if needed.
         */
        if (!spokeLast) {
            synchronized (serverController.getUserControllers()) {
                for (UserController v : serverController.getUserControllers()) {
                    if (v.equals(this)) {
                        v.setSpokeLast(true);
                    } else {
                        v.setSpokeLast(false);
                    }
                }
            }
        }
        // The message is returned.
        return new Message(message.getType(), messageContent, message.isPut(),
                message.getFont(), message.getColor());
    }

    /**
     * Broadcast the packet to the specified recipients.
     * 
     * @param packet
     *            the packet
     * @param broadcastFlag
     *            specifies the group of recipients.
     */
    public void broadcast(final Object o, Broadcast broadcastFlag) {
        if (o instanceof Packet) {
            Packet packet = (Packet) o;
            UserController userController;
            if (broadcastFlag == Broadcast.ONLYSERVER) {

                if (packet instanceof Message) {
                    /**
                     * Print message to server gui
                     */
                    Message message = (Message) packet;
                    serverController.getServerGUI().printMessage(
                            message.getMessage());
                } else if ((packet instanceof Notification)) {
                    // TODO: Logic
                }

            } else if (broadcastFlag == Broadcast.ONLYCLIENT) {
                /**
                 * Send object regardless of class instance
                 */
                sendObject(packet);

            } else if (broadcastFlag == Broadcast.ALLBUTME) {

                if (packet instanceof Message) {
                    /**
                     * Print message to server gui
                     */
                    Message message = (Message) packet;
                    serverController.getServerGUI().printMessage(
                            message.getMessage());
                }

                /**
                 * Broadcast to everyone except this
                 */
                for (int i = 0; i < serverController.getUserControllers()
                        .size(); i++) {
                    synchronized (serverController.getUserControllers()) {
                        if (serverController.getUserControllers().get(i) != this) {
                            userController = (UserController) serverController
                                    .getUserControllers().elementAt(i);
                            userController.sendObject(packet);
                        }
                    }
                }
            } else if (broadcastFlag == Broadcast.ALL) {

                if (packet instanceof Message) {
                    /**
                     * Print message to server gui
                     */
                    Message message = (Message) packet;
                    serverController.getServerGUI().printMessage(
                            message.getMessage());
                }

                /**
                 * Broadcast to everyone!
                 */
                for (int i = 0; i < serverController.getUserControllers()
                        .size(); i++) {
                    synchronized (serverController.getUserControllers()) {
                        userController = (UserController) serverController
                                .getUserControllers().elementAt(i);
                        userController.sendObject(packet);
                    }
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public ServerController getServerController() {
        return serverController;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isSpokeLast() {
        return spokeLast;
    }

    /**
     * Is invoked by the UserListener when an object is received from the client
     * 
     * @param o
     *            the object received
     */
    public void receiveObject(Object o) {
        if (((Packet) o).isPut()) {
            if (((Packet) o) instanceof Message) {
                /*
                 * This is an instance of the message class. We give it a
                 * Message pointer to avoid painful type casting :)
                 */
                Message msg = (Message) o;

                if (msg.getType() == Message.TEXT) {
                    /**
                     * Broadcast messages from the client
                     */
                    if (!msg.getMessage().equals("")) {
                        msg = addNameToMessage(msg);
                        broadcast(msg, Broadcast.ALL);
                    }

                } else if (msg.getType() == Message.USERNAME) {
                    /**
                     * It changes the name of the client and updates the
                     * buddylist for all other clients
                     */
                    name = msg.getMessage(); // set the name

                    broadcast(new Message(
                            Message.TEXT,
                            "<div id=\"online\">*** User " + name
                                    + " is online ***</div>",
                            msg.isPut(), // this field is unchanged by the
                                         // server
                            new Font("Times new Roman", Font.BOLD, 14),
                            ColorPool.HTML_LIME), Broadcast.ALLBUTME);

                    broadcast(new Message(Message.TEXT,
                            "Hello, " + name + ". Welcome to Yoddle",
                            msg.isPut(), // this field is unchanged by the
                                         // server
                            new Font("Comic Sans MS", Font.ITALIC, 14),
                            ColorPool.HTML_NAVY), Broadcast.ONLYCLIENT);

                    sendObject(new Message(Message.LOGGEDIN, "", // empty body
                            msg.isPut(), // this field is unchanged by the
                                         // server
                            null, ColorPool.HTML_BLACK)); // no font

                    serverController.updateBuddyList();
                }
            } // end of message packet ifs
            else if (((Packet) o) instanceof Notification) {
                /*
                 * This is an instance of the notification class. We give it a
                 * Notification pointer to avoid painful type casting.
                 */
                Notification ntf = (Notification) o;

                if (ntf.getType() == Notification.ISTYPING) {
                    ntf.setSourceUser(name);
                    broadcast(ntf, Broadcast.ALLBUTME);
                } else if (ntf.getType() == Notification.STOPPEDTYPING) {
                    ntf.setSourceUser(name);
                    broadcast(ntf, Broadcast.ALLBUTME);
                } else if (ntf.getType() == Notification.ENTEREDTEXT) {
                    ntf.setSourceUser(name);
                    broadcast(ntf, Broadcast.ALLBUTME);
                } else if (ntf.getType() == Notification.REMOVEDTEXT) {
                    ntf.setSourceUser(name);
                    broadcast(ntf, Broadcast.ALLBUTME);
                } else if (ntf.getType() == Notification.SELECTEDSMILEY) {
                    ntf.setSourceUser(name);
                    broadcast(ntf, Broadcast.ALLBUTME);
                }
            }
        } // end of pushed packets
    }

    public void sendObject(Object o) {
        userListener.sendObject(o);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setServerController(ServerController serverController) {
        this.serverController = serverController;
    }

    public void setSpokeLast(boolean spokeLast) {
        this.spokeLast = spokeLast;
    }

    /**
     * Updates your buddylist, it sends the buddylist to the client
     */
    public void updateYourBuddyList() {
        Vector<Object> buddyList = new Vector<Object>();
        synchronized (serverController.getUserControllers()) {
            for (int i = 0; i < serverController.getUserControllers().size(); i++) {
                buddyList.add(new String(serverController.getUserControllers()
                        .get(i).getName()));
            }
        }
        sendObject(new List(buddyList, List.ONLINEUSERS, true));
    }
}
