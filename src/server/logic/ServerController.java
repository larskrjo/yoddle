package server.logic;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import javax.swing.SwingUtilities;

import server.gui.ServerGUI;

/**
 * The ServerController listens for incoming users
 * 
 * @author Larsen
 * 
 */
public class ServerController {

    private ServerSocket serverSocket;
    private int port = 55555;

    private ServerGUI serverGUI;

    private Vector<UserController> userControllers;

    public ServerController() {

        final ServerController serverController = this;

        /**
         * Creates socket
         */
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out
                    .println("Something went wrong when the socket tried to open on port "
                            + port);
            e.printStackTrace();
        }

        /**
         * Starting GUI
         */
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    setServerGUI(new ServerGUI(serverController));
                }
            });
        } catch (InterruptedException e) {
            System.out
                    .println("Something went wrong in the initialization of the server gui.");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out
                    .println("Something went wrong in the initialization of the server gui.");
            e.printStackTrace();
        }

        while (true) {
            try {
                /**
                 * Listens for a connection to be made to this socket and
                 * accepts it. The method blocks until a connection is made.
                 */

                Socket connectionSocket = serverSocket.accept();

                UserController userController = new UserController(
                        connectionSocket, this);

                if (userControllers == null) {
                    userControllers = new Vector<UserController>();
                }

                /**
                 * Adds the user to the vector
                 */
                addUserController(userController);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addUserController(UserController userController) {
        userControllers.add(userController);
        updateBuddyList();
    }

    public int getPort() {
        return port;
    }

    public ServerGUI getServerGUI() {
        return serverGUI;
    }

    public Vector<UserController> getUserControllers() {
        return userControllers;
    }

    public void removeUserController(UserController userController) {
        if (userControllers.contains(userController)) {
            userControllers.remove(userController);
        }
    }

    public void setServerGUI(ServerGUI serverGUI) {
        this.serverGUI = serverGUI;
    }

    /**
     * Update all the clients with updated info about users online, when invoked
     */
    public void updateBuddyList() {
        synchronized (userControllers) {
            for (Object controller : userControllers) {
                ((UserController) controller).setSpokeLast(false);
                ((UserController) controller).updateYourBuddyList();
            }
        }
    }
}
