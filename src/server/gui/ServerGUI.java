package server.gui;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import server.logic.ServerController;

import common.Misc;
import common.YoddleTray;

/**
 * The window the server displays
 * 
 * @author Larsen, Håvard
 * 
 */
public class ServerGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    // The textarea where all text comes
    private JTextArea textarea;
    private URL trayImageURL;
    private ImageIcon trayImageIcon;

    /**
     * Initializes the GUI for the server
     * 
     * @param serverController
     *            is the creator of this
     */
    public ServerGUI(ServerController serverController) {

        trayImageURL = getClass().getResource("/images/serverTrayIcon.png");
        trayImageIcon = new ImageIcon(trayImageURL);

        if (Misc.javaversion >= 6) {
            new YoddleTray(this, trayImageIcon, true);
        }

        // Initialize the textarea
        textarea = new JTextArea();
        textarea.setEditable(false);

        /*
         * Sets the icon for the JFrame. However, there seems to be a problem
         * with the alpha channel in the JFrame. The PNG image is not displayed
         * as a transparent image. :/
         */
        setIconImage(trayImageIcon.getImage());

        // Position the textarea
        setLayout(new BorderLayout());
        add(new JScrollPane(textarea), BorderLayout.CENTER);

        // Other gui spesific stuff
        setSize(500, 300);
        setTitle("Yoddle Server");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        // Start the window at center of the screen
        setLocationRelativeTo(null);

        // Prints the startup message for the server
        textarea.append("Server started at " + new Date() + " on port "
                + serverController.getPort() + "\n");

        setVisible(true);
    }

    public JTextArea getTextarea() {
        return textarea;
    }

    /**
     * Write text to the server gui
     * 
     * @param message
     *            the last message
     */
    public void printMessage(final String message) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                textarea.append(new Date() + " " + message + "\n");
                textarea.setCaretPosition(textarea.getDocument().getLength());
            }
        });
    }
}
