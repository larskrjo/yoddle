package finalstatic;

import java.awt.Color;

/**
 * 
 * @author Håvard, Larsen
 */
public class ColorPool {

    public final static Color LIGHTBLUE = new Color(230, 240, 255);
    public final static Color LIGHTPURPLE = new Color(220, 230, 255);
    public final static Color BLUEGREY = new Color(146, 179, 232);
    public final static Color YODDLE_RED = new Color(198, 24, 0);
    public final static Color YODDLE_BLUE = new Color(24, 69, 173);
    public final static Color YODDLE_GREEN = new Color(24, 162, 33);
    public final static Color YODDLE_YELLOW = new Color(255, 207, 0);

    public final static String HTML_AQUA = "aqua";
    public final static String HTML_BLACK = "black";
    public final static String HTML_BLUE = "blue";
    public final static String HTML_FUCHSIA = "fuchsia";
    public final static String HTML_GRAY = "gray";
    public final static String HTML_GREEN = "green";
    public final static String HTML_LIME = "lime";
    public final static String HTML_MAROON = "maroon";
    public final static String HTML_NAVY = "navy";
    public final static String HTML_OLIVE = "olive";
    public final static String HTML_PURPLE = "purple";
    public final static String HTML_RED = "red";
    public final static String HTML_SILVER = "silver";
    public final static String HTML_TEAL = "teal";
    public final static String HTML_WHITE = "white";
    public final static String HTML_YELLOW = "yellow";
}
