package finalstatic;

public class CSS {

    public final static String chatRoom = ""
            + "<STYLE type=\"text/css\">"
            + "h1#welcome {border-width: 1; border: solid; text-align: center}"
            + "div {border-width: 1; border: solid; text-align: left}"
            + "div#nickname {border-width: 1; border: solid; text-align: left; color: black}"
            +

            "div#message {"
            + "border-width: 1;"
            + "border: solid;"
            + "text-align: left;"
            + "color: #black;"
            + "padding-left: 7px;"
            + "}"
            +

            "div#online {border-width: 1; border: solid; text-align: center; color: green}"
            + "div#offline {border-width: 1; border: solid; text-align: center; color: purple}"
            + "H1 {border-width: 1; border: solid; text-align: center}"
            + "</STYLE>";

    public final static String statusbar = "" + "<STYLE type=\"text/css\">"
            + "div {border-width: 1; border: solid; text-align: left}"
            + "H1 {border-width: 1; border: solid; text-align: center}"
            + "</STYLE>";
}