package finalstatic;

public enum Broadcast {
    ALL, ALLBUTME, ONLYCLIENT, ONLYSERVER;
}
