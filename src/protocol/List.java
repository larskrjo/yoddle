package protocol;

import java.util.Vector;

/**
 * List, which extends HGLJ
 * 
 * @author Larsen
 * 
 */
public class List extends Packet {

    private static final long serialVersionUID = 1L;

    /**
     * The list contains the names of all online users
     */
    public static final int ONLINEUSERS = 0;

    private Vector<Object> objects = new Vector<Object>();
    private int type;
    private boolean put;

    public List(Vector<Object> objects, int type, boolean put) {
        this.objects = objects;
        this.type = type;
        this.put = put;
    }

    /**
     * Returns the objects in the objectsfield.
     * 
     * @return the vector
     */
    public Vector<Object> getObjects() {
        return objects;
    }

    /**
     * See Packet
     */
    @Override
    public int getType() {
        return type;
    }

    /**
     * See Packet
     */
    @Override
    public boolean isPut() {
        return put;
    }
}
