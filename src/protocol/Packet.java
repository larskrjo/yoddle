package protocol;

import java.io.Serializable;

/**
 * The famous Packet protocol. Formerly known as the late but great HGLJ
 * protcol.
 * 
 * @author Larsen, Håvard
 * 
 */
public abstract class Packet implements Serializable {

    private static final long serialVersionUID = 98696044010L;

    /**
     * Returns information about the type of this packet. More specifically it
     * provides a string literal that uniqely determines how the given packet
     * should be interpreted by the server.
     * 
     * Examples of current usage:
     * 
     * Message class: "MESSAGE" (used to send text based messages).
     * 
     * List class: "NAME" (this is for sending or receiving a list of online
     * users)
     * 
     * @return returns a string literal representing the type of this packet.
     */
    public abstract int getType();

    /**
     * Determines whether this object is being put to the server or not. If the
     * object is put, it basically means that it expects no response, it is
     * merely feeding information to the server. Otherwise it is equivalent to a
     * get in that it expects a response object.
     * 
     * @return whether the object is put or not.
     */
    public abstract boolean isPut();
}
