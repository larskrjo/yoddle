package protocol;

import java.awt.Font;

/**
 * The Message class represents all text based messaging objects. It is the
 * simplest form of communication in Yoddle. The content of a message is
 * essentially represented as string literals. A message can .. do much?
 * 
 * @author Larsen
 */
public class Message extends Packet {

    private static final long serialVersionUID = 1L;
    /**
     * The message is a basic text message
     */
    public static final int TEXT = 0;
    /**
     * The message is a username
     */
    public static final int USERNAME = 1;
    /**
     * The message tells the user that it has successfully logged in
     */
    public static final int LOGGEDIN = 2;

    private String message;
    private boolean put;
    private int type;
    private Font font;
    private String color;

    public Message(int type, String message, boolean put, Font font,
            String color) {
        this.message = message;
        this.type = type;
        this.put = put;
        this.font = font;
        this.color = color;
    }

    /**
     * Returns the color of the font
     * 
     * @return the font color
     */
    public String getColor() {
        return color;
    }

    /**
     * Returns the font of the message
     * 
     * @return the font of the message
     */
    public Font getFont() {
        return font;
    }

    /**
     * Returns the message of the protocol
     * 
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * See Packet
     */
    @Override
    public int getType() {
        return type;
    }

    /**
     * See Packet
     */
    @Override
    public boolean isPut() {
        return put;
    }

    /**
     * Allows the protocol to change during the way
     * 
     * @param message
     *            the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
