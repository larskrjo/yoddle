package protocol;

/**
 * A class for sending lightweight notifications to the server about changes in
 * this clients current state.
 * 
 * @author Håvard
 */
public class Notification extends Packet {

    private static final long serialVersionUID = 987654321L;
    /**
     * Notifies the others that this user is typing
     */
    public static final int ISTYPING = 0;
    /**
     * Notifies the others that this user has stopped typing
     */
    public static final int STOPPEDTYPING = 1;
    /**
     * Notifies the others that this user has entered text
     */
    public static final int ENTEREDTEXT = 2;
    /**
     * Notifies the others that this user has entered text
     */
    public static final int REMOVEDTEXT = 3;
    /**
     * Notifies the others that this user has picked a smiley
     */
    public static final int SELECTEDSMILEY = 4;

    private int type;
    private boolean put;
    private String sourceUser;

    /**
     * 
     * @param type
     *            What kind of notification this is. Select among the predefined
     *            class constants from the <code>Notificaion</code> class
     * @param put
     *            Use true if you expect no response from the server, and false
     *            otherwise.
     * @param sourceUser
     *            the user that sent the notification object (temporary)
     */
    public Notification(int type, boolean put, String sourceUser) {
        this.type = type;
        this.put = put;
        this.sourceUser = sourceUser;
    }

    /**
     * Gets the user that sent this notification object.
     * 
     * @return the user that sent this notification object.
     */
    public String getSourceUser() {
        return sourceUser;
    }

    /**
     * Overrides the abstract method from the class <code>Packet</code>.
     * 
     * {@inheritDoc }
     */
    @Override
    public int getType() {
        return type;
    }

    /**
     * Overrides the abstract method from the class <code>Packet</code>.
     * 
     * {@inheritDoc }
     */
    @Override
    public boolean isPut() {
        return put;
    }

    /**
     * This method makes it possible to implement a temporary solution of the
     * notification system. The server identifies a user by its connection, and
     * sets the correct name accordingly before broadcasting it.
     * 
     * @param sourceUser
     *            the user who sent the notification object.
     */
    public void setSourceUser(String sourceUser) {
        this.sourceUser = sourceUser;
    }
}
