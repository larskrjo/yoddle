package common;

import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/*
 * An Alert class that can be used to make a JFrame to blink
 */
public final class Alert {

    static class WindowAlerter {
        Dialog d;

        /**
         * It flashes the window's taskbar icon if the window is not focused.
         * The flashing "stops" when the window becomes focused.
         **/
        public void flash(final Window w) {
            d = new Dialog(w);
            d.setUndecorated(true);
            d.setSize(0, 0);
            d.setModal(false);

            d.addWindowFocusListener(new WindowAdapter() {
                @Override
                public void windowGainedFocus(WindowEvent e) {
                    w.requestFocus();
                    d.setVisible(false);
                    super.windowGainedFocus(e);
                }
            });
            w.addWindowFocusListener(new WindowAdapter() {
                @Override
                public void windowGainedFocus(WindowEvent e) {
                    d.setVisible(false);
                    super.windowGainedFocus(e);
                }
            });

            if (!w.isFocused()) {
                d.setVisible(false);
                d.setLocation(0, 0);
                d.setLocationRelativeTo(w);
                d.setVisible(true);
            }
        }
    }

    // Alerter object used for alerts on windows
    static WindowAlerter alerter;

    // Force a window to flash if not focused
    public static void AlertOnWindow(JFrame frm) {
        try {
            if (alerter == null) {
                alerter = new WindowAlerter();
            }
            alerter.flash(frm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
