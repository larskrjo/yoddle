/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swinghelper.tray.JXTrayIcon;

/**
 * 
 * @author Håvard
 */
public class YoddleTray {

    private boolean noticedUser;
    private final JXTrayIcon trayIcon;
    private ActionListener exitListener, openListener;
    private JPopupMenu popupMenu;
    private JMenuItem openItem, exitItem;
    private JFrame frame;

    public YoddleTray(final JFrame frame, final ImageIcon trayImageIcon,
            final boolean clientSide) {

        this.frame = frame;
        trayIcon = new JXTrayIcon(trayImageIcon.getImage());
        noticedUser = false;

        initTrayIcon();

        /*
         * Using the WindowAdapter class allows us to choose which features to
         * implement, not having to list them all up :)
         */
        frame.addWindowListener(new WindowAdapter() {

            /**
             * If the user is closing the window for the first time, give the
             * user a message so the user know that the program is still running
             */
            @Override
            public void windowClosing(WindowEvent e) {
                if (!noticedUser) {
                    if (clientSide) {
                        trayIcon.displayMessage("Yoddle still running!",
                                "Yoodle is still running on your system",
                                TrayIcon.MessageType.INFO);
                    } else {
                        trayIcon.displayMessage(
                                "Yoddle server still running!",
                                "Yoodle server is still running on your system",
                                TrayIcon.MessageType.INFO);
                    }

                }
                noticedUser = true;
            }
        });

    }

    /**
     * Initializes the trayIcon and its belonging properties.
     */
    private void initTrayIcon() {
        try {
            if (SystemTray.isSupported()) {
                SystemTray systemTray = SystemTray.getSystemTray();

                // makes the program close if triggered
                exitListener = new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                };

                // makes the window visible if triggered
                openListener = new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        frame.setVisible(true);
                    }
                };

                // Instantiate popupMenu.
                popupMenu = new JPopupMenu();
                // Create items for popupMenu.

                exitItem = new JMenuItem("Exit");
                openItem = new JMenuItem("Open");

                // Add listeners to items.
                openItem.addActionListener(openListener);
                exitItem.addActionListener(exitListener);

                // Add items to popupMenu.
                popupMenu.add(openItem);
                popupMenu.add(exitItem);

                // Associate popupMenu with trayIcon.
                trayIcon.setJPopupMenu(popupMenu);

                // Inserts trayIcon into the system tray.
                systemTray.add(trayIcon);

                // Makes the trayIcon scale to fit its availible space.
                trayIcon.setImageAutoSize(true);

                // Makes the frame pop up by double clicking trayIcon.
                trayIcon.addActionListener(openListener);
            }
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
}
